//
//  ContactSettingsViewController.swift
//  SecureSMS
//
//  Created by cris on 11/02/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit

class ContactSettingsViewController: UIViewController, UITextFieldDelegate {

    var contact: String!
    @IBOutlet var usernameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTF.text = ChatManager.passwords[contact]
        usernameTF.text = contact
    }

}

// MARK : - text field delegate
extension ContactSettingsViewController {
    
    func textFieldDidEndEditing(textField: UITextField) {
        var passwords = ChatManager.passwords
        if textField == passwordTF {
            passwords[contact] = self.passwordTF.text
        }else if textField == usernameTF {
            passwords.removeValueForKey(contact)
            contact = self.usernameTF.text
            passwords[contact] = self.passwordTF.text
        }
        ChatManager.passwords = passwords
        print(ChatManager.passwords)
    }
}

//
//  MessagesViewController.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class MessagesViewController: UITableViewController {
    
    var messages: [Message] = []
    var reductedMessages: [Message] = []
    var selectedContact: String?
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadContent()
    }
    
    func reloadContent() {
        ChatManager.manager.fetchMessages(){
            self.messages = ChatManager.manager.retrieveMessagesFromDatabse(SecureChatAPI.loggedUser!)!
            self.reductedMessages  = self.messages.reduce(self.messages) { (var groupedMessages: [Message], message: Message) -> [Message] in
                let filtered = groupedMessages.filter({(mesg: Message) in
                    
                    if mesg != message{
                        if message.sender == SecureChatAPI.loggedUser! {
                            return mesg.sender == message.receiver || mesg.receiver == message.receiver
                        }else{
                            return mesg.sender == message.sender || mesg.receiver == message.sender
                        }
                    }
                    
                    return false
                })
                
                if (filtered.count > 0) { groupedMessages.removeObject(message)}
                return groupedMessages
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetails" {
            let destinationVC = segue.destinationViewController as! ChatViewController
            destinationVC.contactPerson = selectedContact
            let messagesForSelectedPerson = messages.filter(){ message in
                return message.receiver == selectedContact || message.sender == selectedContact
            }
            
            var ret: [JSQMessage] = []
            let _ = messagesForSelectedPerson.map({ message in
                if let _ = message.date {
                   ret += [JSQMessage(senderId: message.sender, senderDisplayName: message.sender, date: message.date, text: message.message)]
                }

            })
            destinationVC.messages = ret
        }
    }
    
    @IBAction func addBtnClicked(sender: AnyObject) {
        let alert = UIAlertController(title: "Add message", message: "Select contact name", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let contacts = ((ChatManager.passwords as NSDictionary).allKeys as! [String]).filter({ contact in
            for message in reductedMessages {
                if (message.sender == contact || message.receiver == contact) { return false }
            }
            return true
        })
        for contact in contacts {
            alert.addAction(UIAlertAction(title: contact, style: .Default, handler: {action in
                let message = Message.MR_createEntity()
                message?.receiver = action.title!
                message?.sender = SecureChatAPI.loggedUser
                message?.owner = SecureChatAPI.loggedUser
                self.messages += [message!]
                dispatch_async(dispatch_get_main_queue()) {
                    self.reloadContent()
                }
            }))
        }
        
        presentViewController(alert, animated: true, completion: nil)
        
    }
}

// MARK - table view controller delagate
extension MessagesViewController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reductedMessages.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("messagesCell")!
        if let s = (reductedMessages[indexPath.row] as Message).sender where s != SecureChatAPI.loggedUser {
            cell.textLabel?.text = s
        }else{
            cell.textLabel?.text = (reductedMessages[indexPath.row] as Message).receiver

        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let s = (reductedMessages[indexPath.row] as Message).sender where s != SecureChatAPI.loggedUser {
            selectedContact = s
        }else{
            selectedContact = (reductedMessages[indexPath.row] as Message).receiver
            
        }
        performSegueWithIdentifier("showDetails", sender: self)
    }
    
    
}

extension Array {
    
    mutating func removeObject<U: AnyObject>(object: U) -> Element? {
        if count > 0 {
            for index in startIndex ..< endIndex {
                if (self[index] as! U) === object { return self.removeAtIndex(index) }
            }
        }
        return nil
    }
}

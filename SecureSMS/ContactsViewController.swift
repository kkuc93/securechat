//
//  ContactsViewController.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit

class ContactsViewController: UITableViewController {

    var contacts: [String] = []
    var selectedContact: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        contacts = ((ChatManager.passwords as NSDictionary).allKeys as! [String]).filter(){ contact in
            return contact != SecureChatAPI.loggedUser
        }
        tableView.reloadData()
    }
    
    @IBAction func addBtnClicked(sender: AnyObject) {
        let alert = UIAlertController(title: "Add contact", message: "Type contact name", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { textfield in
            textfield.placeholder = "Contact"
        }
        alert.addAction(UIAlertAction(title: "Add", style: .Default, handler: { action in
            self.contacts += [(alert.textFields![0] as UITextField).text!]
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }))
        
        presentViewController(alert, animated: true, completion: nil)
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showContact" {
            let destinationVC = segue.destinationViewController as! ContactSettingsViewController
            destinationVC.contact = selectedContact
        }
    }

}


// MARK : - table view delegate
extension ContactsViewController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("contactCell")
        cell?.textLabel?.text = contacts[indexPath.row]
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedContact = contacts[indexPath.row]
        performSegueWithIdentifier("showContact", sender: self)
    }
    
}

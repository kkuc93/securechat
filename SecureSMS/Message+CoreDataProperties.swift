//
//  Message+CoreDataProperties.swift
//  SecureSMS
//
//  Created by cris on 02/02/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Message {

    @NSManaged var sender: String?
    @NSManaged var receiver: String?
    @NSManaged var message: String?
    @NSManaged var date: NSDate?
    @NSManaged var owner: String?

}

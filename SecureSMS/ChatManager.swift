//
//  ChatManager.swift
//  SecureSMS
//
//  Created by cris on 02/02/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MagicalRecord
import CryptoSwift

class ChatManager{
    
    private static let kPasswords = "passwords"
    static let manager = ChatManager()
    static var passwords: [String:String]! {
        get {
            if let p = NSUserDefaults.standardUserDefaults().objectForKey(kPasswords){
                return p  as! [String : String]
            }
            self.passwords = [:]
            return NSUserDefaults.standardUserDefaults().objectForKey(kPasswords) as! [String: String]
        }
        
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: kPasswords)
        }
    }
    
    func fetchMessages(completion: () -> Void){
        if let user = SecureChatAPI.loggedUser{
            Alamofire.request(.GET, "\(SecureChatAPI.hostName)\(SecureChatAPI.messageFetchPath)\(user)")
                .response{ (request, response, data, error) in
                    if response?.statusCode == 200{
                        let jsonResponse = JSON(data: data!)
                        for item in jsonResponse{
                            
                            var message: String? = item.1["message"].string
                            if let pass = ChatManager.passwords[item.1["sender"].string!]{
                                //decrypt message
                                let keyData = pass.dataUsingEncoding(NSUTF8StringEncoding)
                                let encData = NSData(base64EncodedString: item.1["message"].string!, options: NSDataBase64DecodingOptions(rawValue: 0))
                                do{
                                    let dec = try AES(key: (keyData?.arrayOfBytes())!).decrypt((encData?.arrayOfBytes())!)
                                    let decData = NSData(bytes: dec)
                                    message = String(data: decData, encoding: NSUTF8StringEncoding)
                                }catch{
                                    message = item.1["message"].string
                                }
                            }
                            
                            
                            let msg = Message.MR_createEntity()
                            msg?.receiver = item.1["receiver"].string
                            msg?.sender = item.1["sender"].string
                            msg?.message = message
                            msg?.date = NSDate()
                            msg?.owner = SecureChatAPI.loggedUser
                            NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
                        }
                    }
                    completion()
            }
        }
    }
    
    func sendMessage(receiver: String, message: String, sender: String, completion: (response: NSHTTPURLResponse?, error: NSError?) -> Void){
        
        //encrypt message
        var encryptedMessage: String? = message
        if let pass = ChatManager.passwords[SecureChatAPI.loggedUser!]{
            do{
                let data = message.dataUsingEncoding(NSUTF8StringEncoding)
                let keyData = pass.dataUsingEncoding(NSUTF8StringEncoding)
                let encryptedData = try AES(key: (keyData?.arrayOfBytes())!).encrypt(data!.arrayOfBytes())
                let base64String: String = NSData(bytes: encryptedData).base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0));
                encryptedMessage = String(base64String)
            }catch{
                encryptedMessage = message
            }
        }
                
        Alamofire.request(.POST,
            "\(SecureChatAPI.hostName)\(SecureChatAPI.messageSendPath)",
            parameters: ["receiver" : receiver, "sender": sender, "message": encryptedMessage!],
            encoding: .JSON,
            headers: nil)
            .response{(request, response, data, error) in
                
                //save message to database
                let msg = Message.MR_createEntity()
                msg?.receiver = receiver
                msg?.sender = sender
                msg?.message = message
                msg?.date = NSDate()
                msg?.owner = SecureChatAPI.loggedUser
                
                NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
                
                completion(response: response, error: error)
        }
    }
    
    func retrieveMessagesFromDatabse(person: String) -> [Message]?{
        let messages = (Message.MR_findAll() as! [Message]).filter({ (message: Message) -> Bool in
            return  (message as Message).owner == person
        }) 
        
        return messages
    }

}

//
//  SecureChatAPI.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import Foundation

class SecureChatAPI {
    static let hostName = "http://securemessages-iosserver.rhcloud.com"
    static let loginPath = "/rest/user/login"
    static let registrationPath = "/rest/user/register"
    static let messageFetchPath = "/rest/message/all/"
    static let messageSendPath = "/rest/message/create"
    
    static var loggedUser: String?
}
//
//  SettingsViewController.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTF.text = ChatManager.passwords[SecureChatAPI.loggedUser!]
    }
}

// MARK : - text field delegate
extension SettingsViewController {
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField.text?.characters.count != 16 {
            let alert = UIAlertController(title: "Error", message: "Password must have 16 characters", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }else{
            var passwords = ChatManager.passwords
            passwords[SecureChatAPI.loggedUser!] = self.passwordTF.text
            ChatManager.passwords = passwords
   
        }
    }
}

//
//  LoginViewController.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit
import CryptoSwift
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet var usernameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var loginBTN: UIButton!
    @IBOutlet var registerBTN: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableAll() {
        usernameTF.enabled = false
        passwordTF.enabled = false
        loginBTN.enabled = false
        registerBTN.enabled = false
    }
    
    func enableAll() {
        usernameTF.enabled = true
        passwordTF.enabled = true
        loginBTN.enabled = true
        registerBTN.enabled = true
    }
    
    @IBAction func logIn() {
        disableAll()

        let params = ["username" : usernameTF.text!,
                       "password": ((passwordTF.text?.dataUsingEncoding(NSUTF8StringEncoding))! as NSData).md5().toHexString()]
        
        Alamofire.request(.POST, "\(SecureChatAPI.hostName)\(SecureChatAPI.loginPath)", parameters: params, encoding: .JSON, headers: nil).response { (request, response, data, error) in
            
            if response?.statusCode != 200 {
                let alert = UIAlertController(title: "Log in failed", message: "\(response)", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }else{
                SecureChatAPI.loggedUser = self.usernameTF.text
                ChatManager.manager.fetchMessages(){
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        let appDelegate  = UIApplication.sharedApplication().delegate as! AppDelegate
                        (((appDelegate.window!.rootViewController! as! MainTabBarController).viewControllers![0] as! UINavigationController).viewControllers[0] as! MessagesViewController).reloadContent()
                    })
                }
            }
            
            self.enableAll()
        }

        
    }
    
    @IBAction func register(){
     
        disableAll()
        
        let params = ["username" : usernameTF.text!,
            "password": passwordTF.text!]
        
        Alamofire.request(.POST, "\(SecureChatAPI.hostName)\(SecureChatAPI.registrationPath)", parameters: params, encoding: .JSON, headers: nil).response { (request, response, data, error) in
          
            if response?.statusCode != 200 {
                let alert = UIAlertController(title: "Registration failure", message: "\(response)", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            self.enableAll()
        }


    }

}

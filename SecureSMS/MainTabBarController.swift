//
//  MainTabBarController.swift
//  SecureSMS
//
//  Created by cris on 31/01/16.
//  Copyright © 2016 Sebastian Sokołowski. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    static var didAppearOnce = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        
        //show login controller logic
        if MainTabBarController.didAppearOnce == 0 {
            let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("loginVC") as! LoginViewController
            loginViewController.hidesBottomBarWhenPushed = true
            self.presentViewController(loginViewController, animated: true){
                MainTabBarController.didAppearOnce = 1
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
